<?php
/*
Plugin Name: Nextcom Digital - mu-plugin
Plugin URI: https://nextcom.digital
Description: Customizations for nextcom.digital site (WordPress CPT and CPF)
Author: Nextcom Digital
Version: 1.0.0.1
Author URI: https://nextcom.digital/
Text Domain: nextcomdigital
*/

/**
 * Load Translation
 */
function nextcomdigital_load_textdomain() 
{
    load_muplugin_textdomain('nextcomdigital', basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'nextcomdigital_load_textdomain');

/**
 *
 * Custom Post Types
 *
 */

/**
 * Works
 */
function cpt_nextcom_works() 
{
    $labels = array(
            'name'                  => __('Works', 'nextcomdigital'),
            'singular_name'         => __('Work', 'nextcomdigital'),
            'menu_name'             => __('Works', 'nextcomdigital'),
            'name_admin_bar'        => __('Work', 'nextcomdigital'),
            'archives'              => __('Work Archives', 'nextcomdigital'),
            'attributes'            => __('Work Attributes', 'nextcomdigital'),
            'parent_item_colon'     => __('Parent Work:', 'nextcomdigital'),
            'all_items'             => __('All Works', 'nextcomdigital'),
            'add_new_item'          => __('Add New Work', 'nextcomdigital'),
            'add_new'               => __('Add work', 'nextcomdigital'),
            'new_item'              => __('New Work', 'nextcomdigital'),
            'edit_item'             => __('Edit Work', 'nextcomdigital'),
            'update_item'           => __('Update Work', 'nextcomdigital'),
            'view_item'             => __('View Work', 'nextcomdigital'),
            'view_items'            => __('View Works', 'nextcomdigital'),
            'search_items'          => __('Search Work', 'nextcomdigital'),
            'not_found'             => __('Not found', 'nextcomdigital'),
            'not_found_in_trash'    => __('Not found in Trash', 'nextcomdigital'),
            'featured_image'        => __('Featured Image', 'nextcomdigital'),
            'set_featured_image'    => __('Set featured image', 'nextcomdigital'),
            'remove_featured_image' => __('Remove featured image', 'nextcomdigital'),
            'use_featured_image'    => __('Use as featured image', 'nextcomdigital'),
            'insert_into_item'      => __('Insert into work', 'nextcomdigital'),
            'uploaded_to_this_item' => __('Uploaded to this work', 'nextcomdigital'),
            'items_list'            => __('Works list', 'nextcomdigital'),
            'items_list_navigation' => __('Works list navigation', 'nextcomdigital'),
            'filter_items_list'     => __('Filter Works list', 'nextcomdigital'),
    );
    $rewrite = array(
            'slug'                  => __('works', 'nextcomdigital'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Work', 'nextcomdigital'),
            'description'           => __('Nextcom Works', 'nextcomdigital'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array('category', 'post_tag'),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 7,
            'menu_icon'             => 'dashicons-admin-page',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('nextcom_works', $args); 
} 
add_action('init', 'cpt_nextcom_works', 0);

/**
 * Works Custom Fields
 */
function cmb_nextcom_work() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_work_';
    
    /**
    * Initiate the metabox
    */
    $cmb_works = new_cmb2_box(
        array(
            'id'            => 'nextcomdigital_works',
            'title'         => __('Work', 'nextcomdigital'),
            'object_types'  => array('nextcom_works'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Description
    $cmb_works->add_field( 
        array(
            'name'       => __('Description', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'text',
            'type'       => 'textarea_code',
        )
    );

    //Local
    $cmb_works->add_field( 
        array(
            'name'       => __('Client', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'client',
            'type'       => 'text',
        )
    );

    //Date
    $cmb_works->add_field( 
        array (
            'name'       => __('Date', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'date',
            'type' => 'text',
        ) 
    );

    //Featured Video
    $cmb_works->add_field( 
        array (
            'name' => __('Featured Video', 'nextcomdigital'),
            'desc' => __('Enter a youtube or vimeo URL.', 'nextcomdigital'),
            'id'   => $prefix . 'featured_video',
            'type' => 'oembed',
        ) 
    );

    //Featured Image
    $cmb_works->add_field(
        array (
            'name'        => __('Featured Image', 'nextcomdigital'),
            'description' => '',
            'id'          => $prefix . 'featured_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Gallery Title
    $cmb_works->add_field( 
        array(
        'name' => __('Gallery Title', 'nextcomdigital'),
        'desc' => '',
        'type' => 'textarea_code',
        'id'   => $prefix.'gallery_title',
        )
    );

    //Gallery Text
    $cmb_works->add_field( 
        array(
        'name' => __('Gallery Text', 'nextcomdigital'),
        'desc' => '',
        'id' => $prefix.'gallery_text',
        'type' => 'textarea_code',
        ) 
    );

    //Gallery Images
    $cmb_works->add_field(
        array(
            'name' => __('Images', 'nextcomdigital'),
            'id'   => $prefix.'gallery_images',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'nextcomdigital'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'nextcomdigital'), // default: "Remove Image"
                'file_text' => __('File', 'nextcomdigital'), // default: "File:"
                'file_download_text' => __('Download', 'nextcomdigital'), // default: "Download"
                'remove_text' => __('Remove', 'nextcomdigital'),
                'use_text' => __('Remove', 'nextcomdigital'), // default: "Remove"
                'upload_file'  => __('Use this file', 'nextcomdigital'),
                'upload_files' => __('Use these files', 'nextcomdigital'),
                'remove_image' => __('Remove Image', 'nextcomdigital'),
                'remove_file'  => __('Remove', 'nextcomdigital'),
                'file'         => __('File:', 'nextcomdigital'),
                'download'     => __('Download', 'nextcomdigital'),
                'check_toggle' => __('Select / Deselect All', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(320, 180) //'large', // Image size to use when previewing in the admin.
        )
    );
}
add_action('cmb2_admin_init', 'cmb_nextcom_work');

/**
 * Solutions
 */
function cpt_nextcom_solutions() 
{
    $labels = array(
            'name'                  => __('Solutions', 'nextcomdigital'),
            'singular_name'         => __('Solution', 'nextcomdigital'),
            'menu_name'             => __('Solutions', 'nextcomdigital'),
            'name_admin_bar'        => __('Solution', 'nextcomdigital'),
            'archives'              => __('Solution Archives', 'nextcomdigital'),
            'attributes'            => __('Solution Attributes', 'nextcomdigital'),
            'parent_item_colon'     => __('Parent Solution:', 'nextcomdigital'),
            'all_items'             => __('All Solutions', 'nextcomdigital'),
            'add_new_item'          => __('Add New Solution', 'nextcomdigital'),
            'add_new'               => __('Add solution', 'nextcomdigital'),
            'new_item'              => __('New Solution', 'nextcomdigital'),
            'edit_item'             => __('Edit Solution', 'nextcomdigital'),
            'update_item'           => __('Update Solution', 'nextcomdigital'),
            'view_item'             => __('View Solution', 'nextcomdigital'),
            'view_items'            => __('View Solutions', 'nextcomdigital'),
            'search_items'          => __('Search Solution', 'nextcomdigital'),
            'not_found'             => __('Not found', 'nextcomdigital'),
            'not_found_in_trash'    => __('Not found in Trash', 'nextcomdigital'),
            'featured_image'        => __('Featured Image', 'nextcomdigital'),
            'set_featured_image'    => __('Set featured image', 'nextcomdigital'),
            'remove_featured_image' => __('Remove featured image', 'nextcomdigital'),
            'use_featured_image'    => __('Use as featured image', 'nextcomdigital'),
            'insert_into_item'      => __('Insert into solution', 'nextcomdigital'),
            'uploaded_to_this_item' => __('Uploaded to this solution', 'nextcomdigital'),
            'items_list'            => __('Solutions list', 'nextcomdigital'),
            'items_list_navigation' => __('Solutions list navigation', 'nextcomdigital'),
            'filter_items_list'     => __('Filter Solutions list', 'nextcomdigital'),
    );
    $rewrite = array(
            'slug'                  => __('solutions', 'nextcomdigital'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Solution', 'nextcomdigital'),
            'description'           => __('Nextcom Solutions', 'nextcomdigital'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 8,
            'menu_icon'             => 'dashicons-admin-page',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('nextcom_solutions', $args); 
} 
add_action('init', 'cpt_nextcom_solutions', 0);

/**
 * Solutions Custom Fields
 */
function cmb_nextcom_solution() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_solution_';
    
    /**
    * Initiate the metabox
    */
    $cmb_home = new_cmb2_box(
        array(
            'id'            => 'nextcomdigital_solutions',
            'title'         => __('Home', 'nextcomdigital'),
            'object_types'  => array('nextcom_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );
    
    //Home Image
    $cmb_home->add_field( 
        array(
            'name'        => __('Image in Home', 'nextcomdigital'),
            'description' => '',
            'id'          => $prefix . 'home_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Home Text
    $cmb_home->add_field( 
        array(
            'name'       => __('Text in Home', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'home_text',
            'type'       => 'textarea_code',
        )
    );

    /**
    * Initiate the metabox
    */
    $cmb_solutions = new_cmb2_box(
        array(
            'id'            => 'nextcomdigital_solutions',
            'title'         => __('Solution Classes', 'nextcomdigital'),
            'object_types'  => array('nextcom_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Classes Group
    $solutions_id = $cmb_solutions->add_field( 
        array(
            'id'          => $prefix . 'classes',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Class {#}', 'nextcomdigital'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Class', 'nextcomdigital'),
                'remove_button' => __('Remove Class', 'nextcomdigital'),
                'sortable'      => true, // beta
            ),
        )
    );
    
    //Class Image
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name'        => __('Class Image', 'nextcomdigital'),
            'description' => '',
            'id'          => 'class_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Picture', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Class Name
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name' => __('Class Name', 'nextcomdigital'),
            'id'   => 'class_name',
            'type' => 'text',
        ) 
    );

    //Class Description
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
        'name' => __('Class Description', 'nextcomdigital'),
        'id'   => 'class_desc',
        'description' => '',
        'type' => 'textarea_code',
        ) 
    );

    /**
    * Initiate the metabox
    */
    $cmb_solution = new_cmb2_box(
        array(
            'id'            => 'nextcomdigital_solutions',
            'title'         => __('Solution', 'nextcomdigital'),
            'object_types'  => array('nextcom_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Section Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Solution Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'solution_title',
            'type'       => 'textarea_code',
        )
    );

    //Description Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Description Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'desc_title',
            'type'       => 'textarea_code',
        )
    );

    //Description
    $cmb_solution->add_field( 
        array(
            'name'       => __('Description', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'text',
            'type'       => 'textarea_code',
        )
    );

    //Portfolio Title
    $cmb_solution->add_field( 
        array(
            'name'       => __('Portfolio Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'portfolio_title',
            'type'       => 'textarea_code',
        )
    );
    
    //Catchphrase
    $cmb_solution->add_field( 
        array(
            'name'       => __('Catchphrase', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'catchphrase',
            'type'       => 'text',
        )
    );

    //Catchphrase Author
    $cmb_solution->add_field( 
        array(
            'name'       => __('Catchphrase Author', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'catchphrase_author',
            'type'       => 'text',
        )
    );

    //Contato CTA
    $cmb_solution->add_field( 
        array(
            'name'       => __('Contact CTA', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_nextcom_solution');

/**************************
 * Front-page Custom Fields
 **************************/
function cmb_nextcom_frontpage()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_frontpage_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'frontpage_hero_id',
            'title'         => __('Hero', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'textarea_code',
        )
    );

    //Hero Text
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Text', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'hero_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * About
     ******/
    $cmb_about = new_cmb2_box(
        array(
            'id'            => 'frontpage_about_id',
            'title'         => __('About', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //About Title
    $cmb_about->add_field( 
        array(
            'name'       => __('About Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'about_title',
            'type'       => 'textarea_code',
        )
    );

    //About Text
    $cmb_about->add_field( 
        array(
            'name'       => __('About Text', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'about_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Portfolio
     ******/
    $cmb_portfolio = new_cmb2_box(
        array(
            'id'            => 'frontpage_portfolio_id',
            'title'         => __('Portfolio', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Portfolio Title
    $cmb_portfolio->add_field( 
        array(
            'name'       => __('Portfolio Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'portfolio_title',
            'type'       => 'textarea_code',
        )
    );

    /******
     * News
     ******/
    $cmb_news = new_cmb2_box(
        array(
            'id'            => 'frontpage_news_id',
            'title'         => __('News', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //News Title
    $cmb_news->add_field( 
        array(
            'name'       => __('News Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'news_title',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Contact
     ******/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'frontpage_contact_id',
            'title'         => __('Contact', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'contact_title',
            'type'       => 'textarea_code',
        )
    );

    //Contact CTA
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact CTA', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Footer
     ******/
    $cmb_footer = new_cmb2_box(
        array(
            'id'            => 'frontpage_footer_id',
            'title'         => __('Footer', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on' => array( 'key' => 'page-template', 'value' => 'views/front-page.blade.php' ),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Footer Text
    $cmb_footer->add_field( 
        array(
            'name'       => __('Footer Text', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'footer_text',
            'type'       => 'textarea_code',
        )
    );

    //Footer Contact
    $cmb_footer->add_field( 
        array(
            'name'       => __('Footer Contact', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'footer_contact',
            'type'       => 'title',
        )
    );

    //Footer Address
    $cmb_footer->add_field( 
        array(
            'name'       => __('Address', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'footer_address',
            'type'       => 'textarea_code',
        )
    );

    //Footer Phone
    $cmb_footer->add_field( 
        array(
            'name'       => __('Phones', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'footer_phones',
            'type'       => 'textarea_code',
        )
    );

    //Footer E-mail
    $cmb_footer->add_field( 
        array(
            'name'       => __('E-mail', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'footer_email',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_nextcom_frontpage');

/**************************
 * About Page Custom Fields
 **************************/
function cmb_nextcom_about()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_about_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'about_hero_id',
            'title'         => __('Hero', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

    /**********
     * Services
     **********/
    $cmb_services = new_cmb2_box(
        array(
            'id'            => 'about_services_id',
            'title'         => __('Services', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Services Title
    $cmb_services->add_field( 
        array(
            'name'       => __('Services Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'services_title',
            'type'       => 'textarea_code',
        )
    );

    //Services Group
    $services_id = $cmb_services->add_field( 
        array(
            'id'          => $prefix.'services',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Service {#}', 'nextcomdigital'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Service', 'nextcomdigital'),
                'remove_button' => __('Remove Service', 'nextcomdigital'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Service Title
    $cmb_services->add_group_field( 
        $services_id, array(
            'name' => __('Title', 'nextcomdigital'),
            'id'   => 'title',
            'type' => 'text',
        ) 
    );

    //Service Description
    $cmb_services->add_group_field( 
        $services_id, array(
        'name' => __('Description', 'nextcomdigital'),
        'id'   => 'desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );
    
    /*********
     * Company
     *********/
    $cmb_company = new_cmb2_box(
        array(
            'id'            => 'about_company_id',
            'title'         => __('Company', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Featured Video
    $cmb_company->add_field( 
        array (
            'name' => __('Featured Video', 'nextcomdigital'),
            'desc' => __('Enter a youtube or vimeo URL.', 'nextcomdigital'),
            'id'   => $prefix . 'company_featured_video',
            'type' => 'oembed',
        ) 
    );

    //Featured Image
    $cmb_company->add_field(
        array (
            'name'        => __('Featured Image', 'nextcomdigital'),
            'description' => '',
            'id'          => $prefix . 'company_featured_image',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Company Title
    $cmb_company->add_field( 
        array(
            'name'       => __('Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'company_title',
            'type'       => 'textarea_code',
        )
    );

    //Company Text
    $cmb_company->add_field( 
        array(
            'name'       => __('Text', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'company_text',
            'type'       => 'textarea_code',
        )
    );

    /******
     * Team
     ******/
    $cmb_team = new_cmb2_box(
        array(
            'id'            => 'about_team_id',
            'title'         => __('Team', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Team Title
    $cmb_company->add_field( 
        array(
            'name'       => __('Team Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'team_title',
            'type'       => 'textarea_code',
        )
    );
    
    //Team Group
    $team_id = $cmb_team->add_field( 
        array(
            'id'          => $prefix.'team',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Team Member {#}', 'nextcomdigital'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Team Member', 'nextcomdigital'),
                'remove_button' => __('Remove Team Member', 'nextcomdigital'),
                'sortable'      => true, // beta
            ),
        )
    );
    
    //Member Image
    $cmb_team->add_group_field( 
        $team_id, array(
            'name'        => __('Profile Picture', 'nextcomdigital'),
            'description' => '',
            'id'          => 'member_picture',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Picture', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                ),
            ),
            'preview_size' => array(320, 180)
        )
    );

    //Member Name
    $cmb_team->add_group_field( 
        $team_id, array(
            'name' => __('Name', 'nextcomdigital'),
            'id'   => 'member_name',
            'type' => 'text',
        ) 
    );

    //Member Occupation
    $cmb_team->add_group_field( 
        $team_id, array(
        'name' => __('Occupation', 'nextcomdigital'),
        'id'   => 'member_occupation',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    //Member Desc
    $cmb_team->add_group_field( 
        $team_id, array(
        'name' => __('Description', 'nextcomdigital'),
        'id'   => 'member_desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    /****************
     * Testimonials
     ***************/
    $cmb_testimonials = new_cmb2_box(
        array(
            'id'            => 'about_testimonials_id',
            'title'         => __('Testimonials', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Testimonials Group
    $testimonials_id = $cmb_testimonials->add_field( 
        array(
            'id'          => $prefix.'testimonials',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Testimonial {#}', 'nextcomdigital'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Entry', 'nextcomdigital'),
                'remove_button' => __('Remove Entry', 'nextcomdigital'),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        )
    );

    //Testimonial Desc
    $cmb_testimonials->add_group_field( 
        $testimonials_id, array(
        'name' => __('Testimonial', 'nextcomdigital'),
        'id'   => 'testimonial',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    //Testimonial Author
    $cmb_testimonials->add_group_field( 
        $testimonials_id, array(
        'name' => __('Author', 'nextcomdigital'),
        'id'   => 'author',
        'description' => '',
        'type' => 'text',
        ) 
    );
    
    /******
     * Contact
     ******/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'about_contact_id',
            'title'         => __('Contact', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-sobre.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact CTA
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact CTA', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'contact_text',
            'type'       => 'textarea_code',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_nextcom_about');

/******************************
 * Solutions Page Custom Fields
 ******************************/
function cmb_nextcom_solutions()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_solutions_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'solutions_hero_id',
            'title'         => __('Hero', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

    /**********
     * Solutions
     **********/
    $cmb_solutions = new_cmb2_box(
        array(
            'id'            => 'solutions_solutions_id',
            'title'         => __('Solutions', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Solutions Title
    $cmb_solutions->add_field( 
        array(
            'name'       => __('Solutions Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'solutions_title',
            'type'       => 'textarea_code',
        )
    );

    //Solutions Group
    $solutions_id = $cmb_solutions->add_field( 
        array(
            'id'          => $prefix.'solutions',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Solution {#}', 'nextcomdigital'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Solution', 'nextcomdigital'),
                'remove_button' => __('Remove Solution', 'nextcomdigital'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Solution Title
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
            'name' => __('Title', 'nextcomdigital'),
            'id'   => 'title',
            'type' => 'text',
        ) 
    );

    //Solution Description
    $cmb_solutions->add_group_field( 
        $solutions_id, array(
        'name' => __('Description', 'nextcomdigital'),
        'id'   => 'desc',
        'description' => '',
        'type' => 'textarea_small',
        ) 
    );

    /***************
     * Solution CTA
     **************/
    $cmb_solutions_cta = new_cmb2_box(
        array(
            'id'            => 'solutions_cta_id',
            'title'         => __('Solutions CTA', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Solution CTA
    $cmb_solutions_cta->add_field( 
        array(
            'name'       => __('Solutions CTA', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'cta_text',
            'type'       => 'textarea_code',
        )
    );

    /**********
     * Clients
     **********/
    $cmb_clients = new_cmb2_box(
        array(
            'id'            => 'solutions_clients_id',
            'title'         => __('Clients', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-solucoes.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Client Group
    $clients_id = $cmb_clients->add_field( 
        array(
            'id'          => $prefix.'clients',
            'type'        => 'group',
            'description' => '', 
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Client {#}', 'nextcomdigital'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Client', 'nextcomdigital'),
                'remove_button' => __('Remove Client', 'nextcomdigital'),
                'sortable'      => true, // beta
            ),
        )
    );

    //Client Name
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name' => __('Name', 'nextcomdigital'),
            'id'   => 'name',
            'type' => 'text',
        ) 
    );

    //Client URL
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name' => __('URL', 'nextcomdigital'),
            'id'   => 'url',
            'type' => 'text_url',
        ) 
    );
    
    //Client Logo
    $cmb_clients->add_group_field( 
        $clients_id, array(
            'name'        => __('Logo', 'nextcomdigital'),
            'description' => '',
            'id'          => 'logo',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Logo', 'nextcomdigital'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                ),
            ),
            'preview_size' => array(180, 180)
        )
    );

}
add_action('cmb2_admin_init', 'cmb_nextcom_solutions');

/******************************
 * Blog Page Custom Fields
 ******************************/
function cmb_nextcom_blog()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_blog_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'blog_hero_id',
            'title'         => __('Hero', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            //'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/home.blade.php'),
            'show_on'       =>  array( 'key' => 'home', 'value' => ''),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

}
add_action('cmb2_admin_init', 'cmb_nextcom_blog');

/******************************
 * Contact Page Custom Fields
 ******************************/
function cmb_nextcom_contact()
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_nextcomdigital_contact_';

    /******
     * Hero
     ******/
    $cmb_hero = new_cmb2_box(
        array(
            'id'            => 'contact_hero_id',
            'title'         => __('Hero', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Hero Title
    $cmb_hero->add_field( 
        array(
            'name'       => __('Hero Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'hero_title',
            'type'       => 'text',
        )
    );

    /**********
     * Contact
     **********/
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'contact_contact_id',
            'title'         => __('Contact', 'nextcomdigital'),
            'object_types'  => array('page'), // post type
            'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/page-contato.blade.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        )
    );

    //Contact Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'title',
            'type'       => 'textarea_code',
        )
    );

    //Contact Text
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Text', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'text',
            'type'       => 'textarea_code',
        )
    );

    //Contact Form Title
    $cmb_contact->add_field( 
        array(
            'name'       => __('Contact Form Title', 'nextcomdigital'),
            'desc'       => '',
            'id'         => $prefix . 'form_title',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_nextcom_contact');
?>
